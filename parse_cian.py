import cianparser


data = cianparser.parse(
    deal_type="sale",
    accommodation_type="flat",
    location="Москва",
    rooms=(1,),
    start_page=1,
    end_page=20,
    is_saving_csv=True,
    is_latin=False,
    is_express_mode=False,
    is_by_homeowner=False,
)


print(data[0])
