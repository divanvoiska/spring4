# syntax=docker/dockerfile:1
   
FROM python:latest
RUN apt-get update && apt-get install python3-pip -y && pip install --upgrade pip && pip install pipenv
RUN apt-get install py3-numpy py3-pandas py3-scipy py3-scikit-learn py3-matplotlib py3-seaborn
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/
COPY . /usr/src/app/
EXPOSE 5000
RUN pip install -r requirements.txt
CMD ["python", "app.py"]

